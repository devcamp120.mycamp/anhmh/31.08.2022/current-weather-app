"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    $("#btn-search").on("click", function () {
        onBtnSearchClick();
    });
    $("#btn-search-forecast").on("click", function () {
        onBtnSearchForeCastClick()
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //Hàm xử lý khi ấn nút "Search" - index.html
    function onBtnSearchClick() {
        "use strict";
        var vCityObj = {
            city: ""
        };
        console.log("Nút Search Được Nhấn!")
        //Bước 0: Khai báo đối tượng
        //Bước 1: Thu thập dữ liệu
        readData(vCityObj);
        //Bước 2: Validate - Kiểm tra dữ liệu
        var vDataValidate = validateData(vCityObj);
        //Bước 3: Ghi dữ liệu ra web
        if (vDataValidate == true) {
            getCurrentWeatherAjaxClick(vCityObj);
            writeData(vCityObj);
        }
    }
    //Hàm gọi Api bằng Ajax - Current Weather
    function getCurrentWeatherAjaxClick() {
        var city = "Ha noi";
        if (city != '') {

            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/weather?q=' + city + "&units=metric" +
                    "&APPID=c10bb3bd22f90d636baa008b1529ee25",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(
                        `Current Weather for ${data.name}, ${data.sys.country}
                        Weather: ${data.weather[0].main}
                        Img: 'http://openweathermap.org/img/w/${data.weather[0].icon}.png'
                        Description: ${data.weather[0].description}
                        Temperature: ${data.main.temp}
                        Pressure: ${data.main.pressure}
                        Humidity: ${data.main.humidity}% 
                        Min Temperature: ${data.main.temp_min}
                        Max Temperature: ${data.main.temp_max}
                        Wind Speed: ${data.wind.speed}
                        Wind Direction: ${data.wind.deg}`
                    );
                    showDataOnHtml(data);
                }


            });
        } else {
            console.error("City not valid")
        }
    }
    //Hàm xử lý khi ấn nút "Search" - forecast.html
    function onBtnSearchForeCastClick() {
        "use strict";
        //Bước 0: Khai báo đối tượng
        var vCityObject = {
            city: "",
            days: ""
        }
        console.log("Nút Search Forecast Được Ấn!");
        //Bước 1: Thu thập dữ liệu
        readDataForeCast(vCityObject);
        //Bước 2: Validate - Kiểm tra dữ 
        var vDataForecastValidate = validateForecastData(vCityObject);
        //Bước 3: Ghi dữ liệu ra web
        if (vDataForecastValidate == true) {
            getWeatherForecastAjaxClick(vCityObject);
            writeForecastData(vCityObject);
        }
    }
    //Hàm gọi Api bằng Ajax - Forecast Weather
    function getWeatherForecastAjaxClick(paramCityObject) {
        var vCity = paramCityObject.city;
        var vDays = paramCityObject.days;

        if (vCity != '' && vDays != '') {
            $.ajax({
                url: 'http://api.openweathermap.org/data/2.5/forecast/daily?q=' + vCity + "&units=metric" +
                    "&cnt=" + vDays + "&APPID=c10bb3bd22f90d636baa008b1529ee25",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data.list);
                    for (var i = 0; i < data.list.length; i++) {
                        var icon = "http://openweathermap.org/img/w/" + data.list[i].weather[0].icon + ".png";
                        var weather = data.list[i].weather[0].main;
                        var description = data.list[i].weather[0].description;
                        var morning_temperature = data.list[i].temp.morn;
                        var night_temperature = data.list[i].temp.night;
                        var min_temperature = data.list[i].temp.min;
                        var max_temperature = data.list[i].temp.max;
                        var pressure = data.list[i].pressure;
                        var humidity = data.list[i].humidity;
                        var speed = data.list[i].speed;
                        var deg = data.list[i].deg;
                    }
                    loadForecastDataToTable(data);
                }
            });
        } else {
            console.error("Input not valid")
        }
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Bước 1 - Đọc dữ liệu khi ấn nút "Search"
    function readData(paramCity) {
        "use strict";
        var vCityElement = $.trim($("#inp-city-name").val());
        paramCity.city = vCityElement;
    }
    //Bước 2 - Kiểm tra dữ liệu khi ấn nút "Search"
    function validateData(paramCity) {
        "use strict";
        if (paramCity.city == "") {
            alert("Bạn vui lòng điền tên thành phố!");
            return false;
        }
        return true;
    }
    //Bước 3 - HIển thị dữ liệu 
    function writeData(paramCity) {
        "use strict";
        console.log("Thành phố " + paramCity.city);
    }
    //Hàm hiển thị dữ liệu dự báo thời tiết lên HTML Log
    function showDataOnHtml(paramData) {
        var vElementDivCurrentWeather = $("#div-container-current-weather");
        var vElementDivHeaderCurrentWeather = $("#div-header-current-weather");
        //Hiển thị vùng Div lên
        vElementDivCurrentWeather.css("display", "block");
        //Hiển thị vùng Header lên 
        vElementDivHeaderCurrentWeather.css("display", "block");
        vElementDivHeaderCurrentWeather.html("<b>Current Weather for </b>" + "<b>" + paramData.name + "</b>" + "," + " " + "<b>" + paramData.sys.country + "</b>" + "<br>");
        //Ghi dữ liệu data và thẻ div
        vElementDivCurrentWeather.append("<b>Weather:</b> " + paramData.weather[0].main + "<br>");
        vElementDivCurrentWeather.append("<b>Description:</b> " + "<img src=image/04d.png>" + paramData.weather[0].description + "<br>");
        vElementDivCurrentWeather.append("<b>Temperature:</b> " + paramData.main.temp + "°C" + "<br>");
        vElementDivCurrentWeather.append("<b>Pressure:</b> " + paramData.main.pressure + "hpa" + "<br>");
        vElementDivCurrentWeather.append("<b>Humidity:</b> " + paramData.main.humidity + "%" + "<br>");
        vElementDivCurrentWeather.append("<b>Min Temperature:</b> " + paramData.main.temp_min + "°C" + "<br>");
        vElementDivCurrentWeather.append("<b>Max Temperature:</b> " + paramData.main.temp_max + "°C" + "<br>");
        vElementDivCurrentWeather.append("<b>Wind Speed:</b> " + paramData.wind.speed + "m/s" + "<br>");
        vElementDivCurrentWeather.append("<b>Wind Direction:</b> " + paramData.wind.deg + "°" + "<br>")
    }
    //Bước 1 - Đọc dữ liệu khi ấn nút "Search" - trang forecast.html
    function readDataForeCast(paramCityObj) {
        "use strict";
        var vCityEle = $.trim($("#inp-city-name").val());
        paramCityObj.city = vCityEle
        var vDaysEle = $.trim($("#inp-next-day").val());
        paramCityObj.days = vDaysEle
    }
    //Bước 2 - Kiểm tra dữ liệu khi ấn nút "Search" - trang forecast.html
    function validateForecastData(paramCityObj) {
        "use strict";
        if (paramCityObj.city == "") {
            alert("Bạn vui lòng điền tên thành phố!");
            return false;
        }
        if (paramCityObj.days == "") {
            alert("Bạn vui lòng điền số lượng ngày!");
            return false;
        }
        return true;
    }
    //Bước 3: Hiển thị dữ liệu - trang forecast.html
    function writeForecastData(paramCityObj) {
        "use strict";
        console.log("Thành phố: " + paramCityObj.city);
        console.log("Số ngày: " + paramCityObj.days);
    }
    //Hàm hiển thị dữ liệu Forecast lên Table trên Html
    function loadForecastDataToTable(paramDataForecast) {
        "use strict";
        for (var i = 0; i < paramDataForecast.list.length; i++) {
            //Tạo row: hàng
            var bNewRow = $("<tr>").appendTo("#weather-forecast-table");
            //Tạo cell: cột các trường
            $("<td>").html(i + 1).appendTo(bNewRow);
            $("<td>").html("<img src=image/10d.png>" + paramDataForecast.list[i].weather[0].icon).appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].weather[0].main).appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].weather[0].description).appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].temp.morn + "°C").appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].temp.night + "°C").appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].temp.min + "°C").appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].temp.max + "°C").appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].pressure + "hpa").appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].humidity + "%").appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].speed + "m/s").appendTo(bNewRow);
            $("<td>").html(paramDataForecast.list[i].deg + "°").appendTo(bNewRow);
        }
    }
});

